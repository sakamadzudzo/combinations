from types import resolve_bases
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QHBoxLayout, QFrame, QWidget, QMessageBox, QSplashScreen, QWidget, QPlainTextEdit, QTextEdit
from PyQt5.QtPrintSupport import QPrinter, QPrintDialog
from PyQt5.QtGui import QPixmap, QMovie, QColor, QPainter, QTextFormat
from PyQt5.QtCore import QObject, QThread, pyqtSlot, QRect, Qt, QRect, QSize, pyqtSignal
# from PyQt5.QtGui import QPainter
import sys
import perm_n_comb_gui
# import number_results
from itertools import combinations
import numpy as np
from collections import Counter
import re
import os, time
import win32api
import win32print
import pyperclip
from itertools import groupby
from operator import itemgetter
import loading
import base64

class ResultListToString(QThread):
    resultString = pyqtSignal(str)

    def __init__(self, resultList, parent=None):
        QThread.__init__(self, parent)
        self.resultList = resultList

    def run(self):
        start_time = time.time()
        result = str(self.resultList)
        end_time = time.time()
        print ("->>>Time to change to string is: ", end_time - start_time)

        self.resultString.emit(result)
        # self.terminate()

class ExampleApp(QtWidgets.QMainWindow, perm_n_comb_gui.Ui_MainWindow):
    def __init__(self, parent=None):
        super(ExampleApp, self).__init__(parent)
        self.setupUi(self)

        self.label_loading.hide()
        self.frame_actions_2.hide()
        self.txt_numbering.hide()
        if os.path.isfile('loading.gif') == False:
            img_dec = base64.b64decode(loading.img_enc)
            output_file = open('loading.gif', 'wb')
            output_file.write(img_dec)
            output_file.close()
        self.btn_generate.clicked.connect(self.showLoadingGif)
        self.btn_generate.clicked.connect(self.generate)
        # self.btn_generate.clicked.connect(self.hideLoadingGif)
        self.btn_clear.clicked.connect(self.clear_results)
        self.btn_delete.clicked.connect(self.delete_numbers)
        self.btn_save.clicked.connect(self.save_results)
        self.btn_search.clicked.connect(self.search_results)
        self.btn_print.clicked.connect(self.print_results)
        self.btn_copy.clicked.connect(self.copy_results)
        self.btn_sort.clicked.connect(self.sort_results)
        # self.btn_cancel.clicked.connect(self.cancel)

    def generate(self):
        start_total_time = time.time()
        start_time = time.time()
        self.txt_edit_results.clear()
        self.txt_edit_results_global.clear()
        self.txt_edit_results_mirror.clear()
        
        if self.txt_number_group.text() != "":
            number_list = self.txt_number_group.text()
            number_list = number_list.split(",")
            number_list = list(map(int, number_list))
        else:
            number_list = np.arange(1, 11).tolist()
        if self.txt_number_group_parameter.text() != "":
            combination_length = int(self.txt_number_group_parameter.text())
        else:
            combination_length = 5
        if self.txt_numbers_to_ignore.text() != "":
            numbers_to_ignore = self.txt_numbers_to_ignore.text()
            numbers_to_ignore = numbers_to_ignore.split(",")
            numbers_to_ignore = list(map(int, numbers_to_ignore))
            for i in list(numbers_to_ignore):
                try:
                    number_list.pop(number_list.index(i))
                except:
                    msg = "Number to ignore is not in the list!"
                    self.error_msg(msg, 2)
        end_time = time.time()
        print ("->>>Time to set up is: ", end_time - start_time)

        start_time = time.time()
        QApplication.processEvents()
        perm = combinations(number_list, combination_length)
        end_time = time.time()
        print ("->>>Time to generate is: ", end_time - start_time)

        start_time = time.time()
        combination_remove_size = 0
        if self.txt_combination_remove_size.text() != "":
            try:
                combination_remove_size = int(self.txt_combination_remove_size.text())
            except:
                msg = "Sequence to remove (size) is not an integer"
                # self.error_msg(msg, 2)

        # multiples_list = []
        multiples = 0
        multiples_appearances = 0
        if self.txt_multiples.text() != "" and self.txt_multiples_appearances.text() != "":
            multiples = self.txt_multiples.text()
            multiples_appearances = self.txt_multiples_appearances.text()
            try:
                multiples = int(multiples)
                multiples_appearances = int(multiples_appearances)
                # multiples_list = [multiples * (x + 1) for x in range(multiples_appearances)]
                # multiples_list = [x for x in number_list if x%multiples == 0]
            except:
                msg = "Either multiples or multiples appearances is not an integer"
                self.error_msg(msg, 2)

        favorite_numbers = []
        if self.txt_favorite_numbers.text() != "":
            try:
                favorite_numbers = self.txt_favorite_numbers.text()
                favorite_numbers = favorite_numbers.split(",")
                favorite_numbers = list(map(int, favorite_numbers))
                if len(favorite_numbers) > len(number_list):
                    favorite_numbers = []
                    msg = "Favorites' list cannot be longer than number group"
                    self.error_msg(msg, 2)
            except:
                msg = "Favorite numbers must be integers"
                self.error_msg(msg, 2)
        
        end_time = time.time()
        print ("->>>Time to make filters is: ", end_time - start_time)

        start_time = time.time()
        # list_perm = [x for x in perm if self.isSubSequence(combination_remove_size, x) if set(multiples_list) <= set(x) if set(favorite_numbers) <= set(x)]
        list_perm = [x for x in perm if self.isSubSequence(combination_remove_size, x) if self.multiplesMatchingAppearanceNum(x, multiples, multiples_appearances) if set(favorite_numbers) <= set(x)]
        end_time = time.time()
        print ("->>>Time to filter is: ", end_time - start_time)

        # start_time = time.time()
        # QApplication.processEvents()
        # string_perm = str(list_perm).replace("[", "").replace("]", "").replace("), (", ")\n(")
        # end_time = time.time()
        # print ("->>>Time to change to string is: ", end_time - start_time)

        self.permToStringThread = ResultListToString(resultList = list_perm)
        self.permToStringThread.resultString.connect(lambda resultString: self.processResultListToStringThreadResult(resultString))
        self.permToStringThread.start()

        end_total_time = time.time()
        print ("->>>Total time is: ", end_total_time - start_total_time)

    def multiplesMatchingAppearanceNum(self, currentIteration, multiples, definedMultiplesMacted):
        if multiples == 0 and definedMultiplesMacted == 0:
            return True
        multiplesMatched = [x for x in currentIteration if x%multiples == 0]
        if len(multiplesMatched) == definedMultiplesMacted:
            return True
        return False
    
    def processResultListToStringThreadResult(self, resultString):
        start_time = time.time()
        QApplication.processEvents()
        if self.txt_edit_results_global.toPlainText() == "":
        # if "[" in resultString:
            string_perm = resultString.replace("[", "").replace("]", "").replace("), (", ")\n(")
            print ("branch 1")
        elif "[[" in resultString:
            string_perm = resultString.replace("[[", "(").replace("]]", ")").replace("[", "(").replace("]", ")").replace("), (", ")\n(")
            print ("branch 1")
        else:
            string_perm = resultString.replace("', '", ")\n(").replace("'", "(").replace("[", "").replace("(]", ")")
            print ("branch 3")
        end_time = time.time()
        print ("->>>Time to change to sort string is: ", end_time - start_time)

        self.txt_edit_results.clear()
        start_time = time.time()
        QApplication.processEvents()
        self.txt_edit_results.insertPlainText(string_perm)
        if self.txt_edit_results_global.toPlainText() == "":
            QApplication.processEvents()
            self.txt_edit_results_global.insertPlainText(string_perm)
            QApplication.processEvents()
            self.txt_edit_results_mirror.insertPlainText(string_perm)
        end_time = time.time()
        print ("->>>Time to print/show results is: ", end_time - start_time)
        self.hideLoadingGif()

    def isSubSequence(self, sequence_remove_size, subList):
        if sequence_remove_size == 0:
            return True
        for k, g in groupby(enumerate(subList), lambda ix : ix[0] - ix[1]):
            some_num = len((list(map(itemgetter(1), g))))
            if some_num >= sequence_remove_size:
                return False
        return True

    def cancel(self):
        self.txt_cancel.setText("1")
        self.txt_edit_results.clear()
        self.txt_edit_results_global.clear()
        self.txt_edit_results_mirror.clear()

    def clear_results(self):
        self.txt_edit_results.clear()
        self.txt_edit_results_global.clear()
        self.txt_edit_results_mirror.clear()
        msg = "Cleared!"
        self.error_msg(msg, 1)
    
    def delete_numbers(self):
        if self.txt_number_group.text() != "" and self.txt_numbers_to_delete.text() != "":
            numbers_to_delete = self.txt_numbers_to_delete.text()
            numbers_to_delete = numbers_to_delete.split(",")
            numbers_to_delete = list(map(int, numbers_to_delete))

            number_list = self.txt_number_group.text()
            number_list = number_list.split(",")
            number_list = list(map(int, number_list))

            for i in list(numbers_to_delete):
                try:
                    number_list.pop(number_list.index(i))
                except:
                    msg = "Number to delete is not in the list!"
                    self.error_msg(msg, 2)
            number_list_str = ""
            for i in list(number_list):
                if number_list_str == "":
                    number_list_str = str(i)
                else:
                    number_list_str = number_list_str + "," + str(i)
            self.txt_number_group.setText(number_list_str)
            self.txt_numbers_to_delete.clear()

    def save_results(self):
        with open('combinations.txt', 'w') as f:
            for item in self.txt_edit_results.toPlainText():
                f.write(item)
        msg = "Results saved to file"
        self.error_msg(msg, 1)
    
    def sort_results(self):
        if self.txt_edit_results_mirror.toPlainText() != "":
            if int(self.txt_sorted.text()) == 1:
                perm = self.txt_edit_results_mirror.toPlainText()
                perm = re.findall("\((.*?)\)", perm)
                perm_temp = [list(map(int, p.split(","))) for p in perm]
                perm_temp.sort()
                self.txt_sorted.setText("0")
                # count = 1
                # line_size = 35
                # self.txt_edit_results.clear()
                # for i in list(perm_temp):
                #     spaces = ""
                #     for ii in range(line_size - int(len(str(i)))):
                #         spaces += "  "
                #     self.txt_edit_results.append(str(i) + spaces + "|   " + str(count))
                #     count += 1
                self.permToStringThread = ResultListToString(resultList = perm_temp)
                self.permToStringThread.resultString.connect(lambda resultString: self.processResultListToStringThreadResult(resultString))
                self.permToStringThread.start()
                msg = "sorted!"
            else:
                perm = self.txt_edit_results_mirror.toPlainText()
                perm = re.findall("\((.*?)\)", perm)
                perm_temp = [list(map(int, p.split(","))) for p in perm]
                perm_temp.sort(reverse=True)
                self.txt_sorted.setText("1")
                # count = 1
                # line_size = 35
                # self.txt_edit_results.clear()
                # for i in list(perm_temp):
                #     spaces = ""
                #     for ii in range(line_size - int(len(str(i)))):
                #         spaces += "  "
                #     self.txt_edit_results.append(str(i) + spaces + "|   " + str(count))
                #     count += 1
                self.permToStringThread = ResultListToString(resultList = perm_temp)
                self.permToStringThread.resultString.connect(lambda resultString: self.processResultListToStringThreadResult(resultString))
                self.permToStringThread.start()
                msg = "Reverse Sorted!"
            self.error_msg(msg, 1)
        else:
            msg = "Generate list first!"
            self.error_msg(msg, 2)

    def is_it_even(self, num):
        if int(num) % 2 == 0:
            return True
        return False

    def count_evens(self, list_to_count, evens_wanted):
        list_to_count = self.clean_x(list_to_count)
        if evens_wanted != -1:
            list_counted  = [x for x in list_to_count if self.is_it_even(x)]
            if len(list_counted) < evens_wanted:
                return False
        return True

    def is_it_odd(self, num):
        if int(num) % 2 != 0:
            return True
        return False

    def count_odds(self, list_to_count, odds_wanted):
        list_to_count = self.clean_x(list_to_count)
        if odds_wanted != -1:
            list_counted  = [x for x in list_to_count if self.is_it_odd(x)]
            if len(list_counted) < odds_wanted:
                return False
        return True

    def search_results(self):
        search_combination = []
        search_combination_bottom = []
        search_combination_top = []
        num_of_bottom = 0
        num_of_top = 0
        len_p = 5
        odds = -1
        evens = -1
        if self.txt_number_group_parameter.text() != "":
            len_p = int(self.txt_number_group_parameter.text())
        if self.txt_edit_results_global.toPlainText() != "" and self.txt_edit_results.toPlainText() != "":
            if self.txt_search.text() != "":
                search_combination = self.txt_search.text()
                search_combination = search_combination.split(",")
                search_combination = list(map(int, search_combination))
            if self.txt_number_of_odds.text() != "":
                odds = int(self.txt_number_of_odds.text())
            
            if self.txt_number_of_evens.text() != "":
                evens = int(self.txt_number_of_evens.text())

            if self.txt_manual_input_bottom_half.text() != "" or self.txt_manual_input_top_half.text() != "":
                if self.txt_items_shown_input_bottom_half.text() != "":
                    num_of_bottom = int(self.txt_items_shown_input_bottom_half.text())
                else:
                    num_of_bottom = -1
                if self.txt_items_shown_input_top_half.text() != "":
                    num_of_top = int(self.txt_items_shown_input_top_half.text())
                else:
                    num_of_top = -1
                if num_of_bottom + num_of_top != len_p and num_of_bottom + num_of_top != -2:
                    msg = "Sum of number of items shown must not exceed number group parameter!"
                    self.error_msg(msg, 3)
                if self.txt_manual_input_bottom_half.text() != "":
                    search_combination_bottom = self.txt_manual_input_bottom_half.text()
                    search_combination_bottom = search_combination_bottom.split(",")
                    search_combination_bottom = list(map(int, search_combination_bottom))
                
                if self.txt_manual_input_top_half.text() != "":
                    search_combination_top = self.txt_manual_input_top_half.text()
                    search_combination_top = search_combination_top.split(",")
                    search_combination_top = list(map(int, search_combination_top))

            self.txt_edit_results.clear()
            perm = self.txt_edit_results_global.toPlainText()
            # self.txt_edit_results_global.clear()
            perm = re.findall("\((.*?)\)", perm)
            perm = [x for x in perm if set(search_combination) <= set(self.clean_x(x)) if self.count_odds(x, odds) if self.count_evens(x, evens) if self.searchHalfHalf(search_combination_top, search_combination_bottom, len_p, num_of_bottom, num_of_top, x)]
            self.permToStringThread = ResultListToString(resultList = perm)
            self.permToStringThread.resultString.connect(lambda resultString: self.processResultListToStringThreadResult(resultString))
            self.permToStringThread.start()
        else:
            msg = "Generate something first!"
            self.error_msg(msg, 3)

    def clean_x(self, x):
        x = x.replace(" ", "").split(",")
        x = list(map(int, x))
        return x

    def searchHalfHalf(self, search_comb_top, search_comb_bottom, len_p, num_of_bottom, num_of_top, current_loop):
        combination_bottom = []
        combination_top = []
        current_loop = current_loop
        current_loop = current_loop.split(",")
        current_loop = list(map(int, current_loop))
        if len(search_comb_top) == 0 and len(search_comb_bottom) == 0:
            return True
        if num_of_bottom != -1 or num_of_top != -1:
            if num_of_bottom != -1:
                combination_bottom = current_loop[0 : int(num_of_bottom)]
                combination_top = current_loop[int(num_of_bottom) : int(len_p)]
                # print ("len - 1:\t",int(len_p - 1))
            elif num_of_top != -1:
                combination_bottom = current_loop[0 : int(len_p - int(num_of_top))]
                combination_top = current_loop[int(len_p - int(num_of_top)) : int(len_p )]
        else:
            if len_p % 2 != 0:
                combination_bottom = current_loop[0 : int((len_p - 1) / 2)]
                combination_top = current_loop[int(((len_p - 1) / 2)) : int(len_p)]
            else:
                combination_bottom = current_loop[0 : int(len_p / 2)]
                combination_top = current_loop[int((len_p / 2)) : int(len_p)]
        # print ("Current loop:\t", current_loop)
        # print ("Combination bottom:\t", combination_bottom)
        # print ("Search combination bottom:\t", search_comb_bottom)
        # print ("Combination top:\t", combination_top)
        # print ("Search combination top:\t", search_comb_top)
        comb_bottom_count = [x for x in combination_bottom if x in search_comb_bottom]
        comb_top_count = [x for x in combination_top if x in search_comb_top]
        # print ("Comb bottom count:\t", comb_bottom_count)
        # print ("Comb top count:\t", comb_top_count)
        # print ("len of comb bottom count", len(comb_bottom_count))
        # print ("len of comb top count", len(comb_top_count))
        if len(comb_bottom_count) >= len(combination_bottom) and len(comb_top_count) >= len(combination_top):
            return True
        # if set(combination_bottom) <= set(search_comb_bottom):
        #     return True
        # if set(combination_top) <= set(search_comb_top):
        #     return True
        return False
    
    def print_results(self):
        QApplication.processEvents()
        printer = QPrinter(QPrinter.HighResolution)
        dialog = QPrintDialog(printer, self)
 
        if dialog.exec_() == QPrintDialog.Accepted:
            if self.txt_edit_results.toPlainText() != "":
                self.txt_edit_results.print_(printer)
            else:
                msg = "Generate list first!"
                self.error_msg(msg, 2)
    
    def copy_results(self):
        if self.txt_edit_results.toPlainText() != "":
            pyperclip.copy(self.txt_edit_results.toPlainText())
            msg = "Copied!"
            self.error_msg(msg, 1)
        else:
            msg = "Generate list first!"
            self.error_msg(msg, 2)
    
    def error_msg(self, msg_text, icon):
        msg = QMessageBox()  #EXCEPTION MESSAGE ONE
        msg.setIcon(int(icon))
        # print (msg_text)
        msg.setText(str(msg_text))
        # msg.setInformativeText("Some informative text")
        if int(icon) == 2 or int(icon) == 3:
            msg.setWindowTitle("Error")
        elif int(icon) == 1:
            msg.setWindowTitle("Message")
        elif int(icon) == 4:
            msg.setWindowTitle("Question")
        # msg.show()
        msg.exec_()

    def subFinder(mylist, pattern):
        matches = []
        for i in range(len(mylist)):
            if mylist[i] == pattern[0] and mylist[i:i+len(pattern)] == pattern:
                matches.append(pattern)
        return matches
    
    def isSubList(mylist, pattern, self):
        for i in range(len(mylist)):
            if mylist[i] == pattern[0] and mylist[i:i+len(pattern)] == pattern:
                return True
        return False

    # Set and play loading gif
    def showLoadingGif(self):
        # QApplication.processEvents()
        self.txt_edit_results.setReadOnly(True)
        self.txt_edit_results.setDisabled(True)
        self.frame_actions.hide()
        # self.frame_actions_2.show()

        QApplication.processEvents()
        self.movie = QMovie('loading.gif')
        QApplication.processEvents()
        self.label_loading.setMovie(self.movie)
        QApplication.processEvents()
        self.label_loading.show()
        QApplication.processEvents()
        self.movie.start()
        # QApplication.processEvents()

    def hideLoadingGif(self):
        self.txt_edit_results.setReadOnly(False)
        self.txt_edit_results.setDisabled(False)
        self.frame_actions.show()
        # self.frame_actions_2.hide()
        self.label_loading.hide()
        self.movie.stop()

def main():
    app = QApplication(sys.argv)
    form = ExampleApp()
    form.show()
    app.exec_()

if __name__ == '__main__':
    perm = []
    main()